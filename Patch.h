#pragma once

#include <QDir>
#include <QFile>
#include <QString>

struct Patch {
    int order;
    bool recommended;
    QString name;
    QString description;
    QDir path;
    QString file;
};

inline bool operator<(Patch const& a, Patch const& b)
{
    return a.order < b.order;
}
