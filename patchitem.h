#pragma once

#include "Patch.h"

#include <QListWidgetItem>

class PatchItem : public QListWidgetItem {
public:
    PatchItem(Patch p, QListWidget* parent = nullptr);

    void check();
    void uncheck();

    bool isChecked() const;
    const Patch& getPatch() const;

private:
    Patch _patch;
};
