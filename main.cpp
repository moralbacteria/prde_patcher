#include "mainwindow.h"

#include <google/vcdecoder.h>

#include <QApplication>
#include <QDir>
#include <QDebug>
#include <QMessageBox>
#include <QProcess>
#include <QXmlStreamReader>

#include <optional>

namespace {

constexpr auto patchesFolder = "patches";
constexpr auto patchMetaFile = "meta.xml";
constexpr auto patchVcdiffFile = "diablo.exe.vcdiff";
constexpr auto fileToPatch = "DIABLO.EXE";
constexpr auto outputFileName = "DIABLO.EXE";
constexpr auto lastPatchConfigFile = "last_patch.txt";

int showError(QString errorText)
{
    QMessageBox messageBox{QMessageBox::Icon::Warning, "Error", errorText, QMessageBox::StandardButton::Ok};
    return messageBox.exec();
}

QByteArray applyPatch(QByteArray before, QByteArray patch)
{
    QByteArray target;
    open_vcdiff::VCDiffDecoder decoder;
    auto const result = decoder.Decode(before.data(), static_cast<size_t>(before.size()),
                   std::string{patch.data(), static_cast<size_t>(patch.size())},
                   &target);
    if (!result) {
        return {};
    }
    return target;
}

QByteArray readFile(QString file, bool text = false)
{
    QFile in{file};
    QIODevice::OpenMode flags = QIODevice::ReadOnly;
    if (text) {
        flags |= QIODevice::Text;
    }
    if (!in.open(flags)) {
        return {};
    }
    return in.readAll();
}

bool writeFile(QString file, QByteArray data)
{
    QFile out{file};
    if (!out.open(QIODevice::WriteOnly)) {
        return false;
    }
    out.write(data.data(), data.size());
    return true;
}


bool writeFile(QString file, QString text)
{
    QFile out{file};
    if (!out.open(QIODevice::WriteOnly | QIODevice::Text)) {
        return false;
    }
    auto utf8Text = text.toUtf8();
    out.write(utf8Text.data(), utf8Text.size());
    return true;
}

struct PatchXmlModel {
    int order;
    bool recommended;
    QString description;
};

std::optional<PatchXmlModel> loadMeta(QString file)
{
    auto metaContents = readFile(file, /*text=*/true);
    if (metaContents.isNull()) {
        return {};
    }

    auto builder = PatchXmlModel{};

    QXmlStreamReader reader{metaContents};
    if (reader.readNextStartElement() && reader.name() != "patch") {
        return {};
    }
    while (reader.readNextStartElement()) {
        if (reader.name() == "order") {
            auto ok = true;
            builder.order = reader.readElementText().toInt(&ok);
            if (!ok) {
                return {};
            }
        } else if (reader.name() == "recommended") {
            builder.recommended = reader.readElementText() == "true";
        } else if (reader.name() == "description") {
            builder.description = reader.readElementText();
        }
    }
    if (reader.hasError()) {
        return {};
    }

    return builder;
}

} // namespace

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    //
    // Validate that patch directory might actually contain patches
    //

    auto const baseDir = QDir{patchesFolder};
    if (!baseDir.exists()) {
        auto const errorMsg = QString{"Patches folder can't be found!\n\nTried: %1"}.arg(baseDir.absolutePath());
        return showError(errorMsg);
    }

    auto const dirEntries = baseDir.entryList(QDir::NoDotAndDotDot | QDir::Dirs);
    if (dirEntries.isEmpty()) {
        auto const errorMsg = QString{"No patches in patches folder!\n\nFolder: %1"}.arg(baseDir.absolutePath());
        return showError(errorMsg);
    }

    if (!baseDir.entryList().contains(fileToPatch)) {
        auto const errorMsg = QString{"Missing base file '%1'!\n\nFolder: %2"}.arg(fileToPatch).arg(baseDir.absolutePath());
        return showError(errorMsg);
    }

    // TODO CRC32 the fileToPatch? to ensure the process will go smoothly

    //
    // Validate each patch for required components (description, vcdiff)
    //

    auto patches = QList<Patch>{};
    for (auto const& dirEntry : dirEntries) {
        auto patchDir = QDir{baseDir};
        if (!patchDir.cd(dirEntry)) {
            auto const errorMsg = QString{"Could not inspect patch `%1`!\n\nFolder: %2"}.arg(dirEntry).arg(patchDir.absolutePath());
            showError(errorMsg);
            continue;
        }

        QFile metaFile{patchDir.filePath(patchMetaFile)};
        if (!metaFile.exists()) {
            auto const errorMsg = QString{"Patch '%1' lacks meta file!\n\nFolder: %2"}.arg(dirEntry).arg(patchDir.absolutePath());
            showError(errorMsg);
            continue;
        }

        QString const vcdiffFilePath = patchDir.filePath(patchVcdiffFile);
        QFile vcdiffFile{vcdiffFilePath};
        if (!vcdiffFile.exists()) {
            auto const errorMsg = QString{"Patch '%1' lacks `%2`!\n\nFolder: %3"}.arg(dirEntry).arg(patchVcdiffFile).arg(patchDir.absolutePath());
            showError(errorMsg);
            continue;
        }

        auto const meta = loadMeta(metaFile.fileName());
        if (!meta) {
            showError(QString{"Failed to parse patch meta: %1\nFolder: %2"}.arg(metaFile.fileName()).arg(patchDir.absolutePath()));
            continue;
        }

        patches << Patch{meta->order, meta->recommended, dirEntry, meta->description, patchDir, vcdiffFilePath};
    }

    MainWindow w{patches};
    w.show();

    auto const lastPatches = readFile(lastPatchConfigFile, /*text=*/true);
    if (!lastPatches.isNull()) {
        w.setCheckedPatchesByName(QString{lastPatches}.split('\n'));
    }

    QObject::connect(&w, &MainWindow::patchesConfirmed, &a, [&baseDir](QList<Patch> patches) {
        // Patches must be applied with some order. Order is defined by the meta file as an absolute int.
        // This works short term, but long term I'd like to specify dependencies and have them resolved automatically.
        std::sort(patches.begin(), patches.end());

        // Load unmodified DIABLO.EXE
        auto patchWip = readFile(baseDir.filePath(fileToPatch));
        if (patchWip.isNull()) {
            showError(QString{"Failed to open base file: %1\nFolder: %2"}.arg(baseDir.filePath(fileToPatch)).arg(baseDir.absolutePath()));
            return;
        }


        QStringList appliedPatchNames;
        for (auto const& patch : patches) {
            auto patchContents = readFile(patch.file);
            if (patchContents.isNull()) {
                showError(QString{"Failed to open patch file: %1\nFolder: %2"}.arg(patch.file).arg(baseDir.absolutePath()));
                return;
            }

            // Apply vcdiff patch iteratively
            patchWip = applyPatch(patchWip, patchContents);
            if (patchWip.isNull()) {
                showError(QString{"Patch failed: %1"}.arg(patch.name));
                return;
            }

            appliedPatchNames << patch.name;
        }

        // Dump to disk
        if (!writeFile(outputFileName, patchWip)) {
            showError(QString{"Failed to write to: %1"}.arg(outputFileName));
            return;
        }

        // Silently fail, the user shouldn't care
        writeFile(lastPatchConfigFile, appliedPatchNames.join('\n'));

        QProcess diablo{};
        diablo.setProgram(outputFileName);
        if (!diablo.startDetached()) {
            showError("Failed to launch process");
        }
    });

    return a.exec();
}
