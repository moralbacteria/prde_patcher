# Diablo PR Demo Enhanced Customizer

A GUI to mix-and-match PR Demo patches. Designed for use with https://gitlab.com/moralbacteria/diablo-prdemo-patches.

## Building

1. Clone recursively.
1. Update submodules recursively
1. Open CMakeLists.txt in Qt Creator (tested with Qt 5.15 MSVC2019)
1. Build

## Deploying (Windows)

Use `windeployqt` (docs here: https://doc.qt.io/qt-5/windows-deployment.html)

1. Build as Release
2. Open cmd.exe
3. run `qtenv2.bat` in Qt install dir
4. cd to build dir
5. `windeployqt --release launcher.exe -dir dist`
6. copy launcher.exe into `dist/`
7. copy patches into `dist/patches/`
8. Copy unmodified DIABLO.EXE into `dist/patches/`
9. Remove `windeployqt` cruft, easy to just run the EXE and remove anything that isn't being used
10. zip up `dist`
