#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QListWidgetItem>
#include <QMainWindow>

#include "Patch.h"
#include "patchitem.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QList<Patch> patches, QWidget *parent = nullptr);
    ~MainWindow();

    void setCheckedPatchesByName(QStringList names);

signals:
    void patchesConfirmed(QList<Patch> patches) const;

private:
    void onPatchItemChanged(QListWidgetItem *current, QListWidgetItem *previous);
    void onApplyButtonClicked();
    void checkAllPatches();
    void uncheckAllPatches();
    void checkRecommendedPatches();
    PatchItem* findPatchItemByName(QString name);

    Ui::MainWindow *ui;
    QList<Patch> _patches;
};
#endif // MAINWINDOW_H
