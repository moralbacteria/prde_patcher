#include "patchitem.h"

PatchItem::PatchItem(Patch p, QListWidget* parent) : QListWidgetItem(parent), _patch{p}
{
    setData(Qt::DisplayRole, _patch.name);
    if (p.recommended) {
        check();
    } else {
        uncheck();
    }
}

void PatchItem::check()
{
    setData(Qt::CheckStateRole, Qt::Checked);
}

void PatchItem::uncheck()
{
    setData(Qt::CheckStateRole, Qt::Unchecked);
}

bool PatchItem::isChecked() const
{
    return data(Qt::CheckStateRole).toBool();
}

const Patch& PatchItem::getPatch() const
{
    return _patch;
}
