#include "mainwindow.h"
#include "./ui_mainwindow.h"

#include "patchitem.h"

MainWindow::MainWindow(QList<Patch> patches, QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
    , _patches{std::move(patches)}
{
    ui->setupUi(this);

    for (auto const& patch : _patches) {
        ui->patchListWidget->addItem(new PatchItem{patch});
    }

    QObject::connect(ui->patchListWidget, &QListWidget::currentItemChanged, this, &MainWindow::onPatchItemChanged);
    QObject::connect(ui->applyButton, &QPushButton::clicked, this, &MainWindow::onApplyButtonClicked);
    QObject::connect(ui->selectAllButton, &QPushButton::clicked, this, &MainWindow::checkAllPatches);
    QObject::connect(ui->selectNoneButton, &QPushButton::clicked, this, &MainWindow::uncheckAllPatches);
    QObject::connect(ui->selectRecommendedButton, &QPushButton::clicked, this, &MainWindow::checkRecommendedPatches);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::setCheckedPatchesByName(QStringList names)
{
    uncheckAllPatches();
    for (const auto& name : names) {
        auto patchItem = findPatchItemByName(name);
        if (!patchItem) {
            continue;
        }
        patchItem->check();
    }
}

void MainWindow::onPatchItemChanged(QListWidgetItem *current, QListWidgetItem *)
{
    // static cast here seems dangerous... although this class can ensure that each QListWidgetItem is-a PatchItem
    auto const patchItem = static_cast<PatchItem*>(current);
    const auto name = patchItem->getPatch().name;
    const auto description =  patchItem->getPatch().description;
    ui->patchDescription->setText(description);
}

void MainWindow::onApplyButtonClicked()
{
    QList<Patch> checkedPatches;
    for (auto row = 0; row < ui->patchListWidget->count(); ++row) {
        auto const patchItem = static_cast<PatchItem*>(ui->patchListWidget->item(row));
        if (patchItem->isChecked()) {
            checkedPatches << patchItem->getPatch();
        }
    }
    emit patchesConfirmed(checkedPatches);
}

void MainWindow::checkAllPatches()
{
    QList<Patch> checkedPatches;
    for (auto row = 0; row < ui->patchListWidget->count(); ++row) {
        auto patchItem = static_cast<PatchItem*>(ui->patchListWidget->item(row));
        patchItem->check();
    }
}

void MainWindow::uncheckAllPatches()
{
    QList<Patch> checkedPatches;
    for (auto row = 0; row < ui->patchListWidget->count(); ++row) {
        auto patchItem = static_cast<PatchItem*>(ui->patchListWidget->item(row));
        patchItem->uncheck();
    }
}

void MainWindow::checkRecommendedPatches()
{
    QList<Patch> checkedPatches;
    for (auto row = 0; row < ui->patchListWidget->count(); ++row) {
        auto patchItem = static_cast<PatchItem*>(ui->patchListWidget->item(row));
        if (patchItem->getPatch().recommended) {
            patchItem->check();
        } else {
            patchItem->uncheck();
        }
    }
}

PatchItem* MainWindow::findPatchItemByName(QString name)
{
    for (auto row = 0; row < ui->patchListWidget->count(); ++row) {
        auto patchItem = static_cast<PatchItem*>(ui->patchListWidget->item(row));
        if (patchItem->getPatch().name == name) {
            return patchItem;
        }
    }
    return nullptr;
}
